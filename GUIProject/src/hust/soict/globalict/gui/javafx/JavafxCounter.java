package hust.soict.globalict.gui.javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;


public class JavafxCounter extends Application {
	private TextField textFieldCount;
	private Button buttonCount;
	private int count = 0;
	
	@Override
	public void start(Stage primaryStage) {
		textFieldCount = new TextField("0");
		textFieldCount.setEditable(false);
		buttonCount = new Button("Count");
		buttonCount.setOnAction(e -> textFieldCount.setText(++count + ""));
		
		FlowPane pane = new FlowPane();
		pane.setPadding(new Insets(15, 12, 15, 12));
		pane.setVgap(10);
		pane.setHgap(10);
		pane.setAlignment(Pos.CENTER);
		pane.getChildren().addAll(new Label("Count: "), textFieldCount, buttonCount);
		
		primaryStage.setScene(new Scene(pane, 400, 80));
		primaryStage.setTitle("JavaFX Counter");
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
