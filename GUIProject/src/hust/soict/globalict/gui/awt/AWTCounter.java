package hust.soict.globalict.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener {
	private Label labelCount;
	private TextField textFieldCount;
	private Button buttonCount;
	private int count = 0;
	
	public AWTCounter() {
		setLayout(new FlowLayout());
		
		labelCount = new Label("Counter");
		add(labelCount);
		
		textFieldCount = new TextField(count + "", 10);
		textFieldCount.setEditable(false);
		add(textFieldCount);
		
		buttonCount = new Button("Count");
		add(buttonCount);
		buttonCount.addActionListener(this);
		
		setTitle("AWT Counter");
		setSize(250, 100);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		AWTCounter app = new AWTCounter();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		++count;
		textFieldCount.setText(count + "");
	}
}
