package hust.soict.globalict.gui.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SwingCounter extends JFrame {
		private static final long serialVersionUID = 1L;
		private JTextField textFieldCount;
		private JButton buttonCount;
		private int count = 0;
		
		public SwingCounter() {
			Container cp = getContentPane();
			cp.setLayout(new FlowLayout());
			
			cp.add(new JLabel("Counter"));
			
			textFieldCount = new JTextField("0");
			textFieldCount.setEditable(false);
			cp.add(textFieldCount);
			
			buttonCount = new JButton("Count");
			cp.add(buttonCount);
			buttonCount.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					++count;
					textFieldCount.setText(count + "");
				}
			});
			
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setTitle("Swing counter");
			setSize(300, 100);
			setVisible(true);
		}
		
		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					new SwingCounter();
				}
			});
		}
}
