package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.*;
import java.util.ArrayList;
import java.util.Collections;

public class TestMediaCompareTo {
	public static void main(String[] args) {
		ArrayList<Media> items = new ArrayList<Media>();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87, 19.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
		
		items.add(dvd1);
		items.add(dvd2);
		items.add(dvd3);
		
		System.out.println("Before sort:");
		for (Media media : items) {
			System.out.println(media.getTitle() + " " + media.getCost());
		}
		
		Collections.sort(items);
		System.out.println();
		System.out.println("After sort:");
		for (Media media : items) {
			System.out.println(media.getTitle() + " " + media.getCost());
		}
	}
}
