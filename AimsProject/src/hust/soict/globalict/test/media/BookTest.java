package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.*;

public class BookTest {
	public static void main(String[] args) {
		Book book = new Book("Abc", "abc");
		book.processContent("Count the frequency of each token, sort by token.");
		System.out.println(book);
	}
}
