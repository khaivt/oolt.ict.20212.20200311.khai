package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
	public static void main(String[] args) {
		MyDate date = new MyDate("twentyfirst", "September", "twenty nineteen");
		date.print();
		
		System.out.println();
		date = new MyDate(25, 2, 2022);
		date.print();
		date.print("yyyy-MM-dd");
		date.print("d/M/yyyy");
		date.print("dd-MMM-yyyy");
		date.print("MMM d yyyy");
		date.print("mm-dd-yyyy");
		
		System.out.println();
		MyDate date1 = new MyDate(24, 2, 2022);
		MyDate date2 = new MyDate(26, 2, 2022);
		if (DateUtils.compare(date1, date2) < 0) {
			System.out.println("Date 1 is before date 2");
		}
		
		System.out.println();
		MyDate[] dates = {new MyDate(19, 3, 2022), new MyDate(25, 2, 2022), new MyDate(10, 3, 2021)};
		DateUtils.sort(dates);
		for (MyDate d : dates) {
			d.print();
		}
	}
}
