package hust.soict.globalict.aims.utils;

import java.util.Scanner;
import java.util.Locale;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	private static String[] list = {"fir", "twe", "th", "fou", "fi", "six", "seve", "eig", "nine"};
	private static String[] months = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
	
	public MyDate() {
		super();
		LocalDate today = LocalDate.now();
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}
	
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	private int parseString(String s) {
		if (s.startsWith("one")) return 1;
		if (s.startsWith("two") || s.startsWith("sec")) return 2;
		if (s.contains("ten")) return 10;
		if (s.contains("elev")) return 11;
		if (s.contains("twev") || s.contains("twel")) return 12;
		int result = 1;
		for (int i = 0; i < 9; i += 1) {
			if (s.startsWith(list[i])) {
				result = i + 1;
				if (s.contains("teen")) {
					result += 10;
					break;
				}
				int from = s.indexOf("ty");
				if (from >= 0) {
					result *= 10;
					s = s.substring(from + 2);
					if (s.startsWith("one")) {
						result += 1;
						break;
					}
					if (s.startsWith("two") || s.startsWith("sec")) {
						result += 2;
						break;
					}
					for (int j = 0; j < 9; j += 1) {
						if (s.startsWith(list[j])) {
							result += j + 1;
							break;
						}
					}
				}
				break;
			}
		}
		return result;
	}
	
	public MyDate(String day, String month, String year) {
		day = day.trim().toLowerCase();
		month = month.trim().toLowerCase();
		year = year.trim().toLowerCase();
		this.day = parseString(day);
		for (int i = 0; i < 12; i += 1) {
			if (month.startsWith(months[i])) {
				this.month = i + 1;
				break;
			}
		}
		int to = year.indexOf(" ");
		this.year = parseString(year.substring(0, to));
		this.year = this.year * 100 + parseString(year.substring(to + 1));
	}
	
	private LocalDate fromString(String dateString) {
		dateString = dateString.replaceFirst("st", "");
		dateString = dateString.replaceFirst("nd", "");
		dateString = dateString.replaceFirst("rd", "");
		dateString = dateString.replaceFirst("th", "");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy", Locale.ENGLISH);
		return LocalDate.parse(dateString, formatter);
	}
	
	public MyDate(String dateString) {
		super();
		LocalDate date = fromString(dateString);
		this.day = date.getDayOfMonth();
		this.month = date.getMonthValue();
		this.year = date.getYear();
	}
	
	public void accept() {
		Scanner scanner = new Scanner(System.in);
		LocalDate date = fromString(scanner.nextLine());
		this.day = date.getDayOfMonth();
		this.month = date.getMonthValue();
		this.year = date.getYear();
		scanner.close();
	}
	
	public void print() {
		LocalDate date = LocalDate.of(year, month, day);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("LLLL d");
		String dateString = date.format(formatter);
		if (day % 10 == 1) dateString += "st";
		else if (day % 10 == 2) dateString += "nd";
		else if (day % 10 == 3) dateString += "rd";
		else dateString += "th";
		dateString += " " + year;
		System.out.println("My date is: " + dateString);
	}
	
	public void print(String format) {
		LocalDate date = LocalDate.of(year, month, day);
		format = format.replaceAll("m", "M");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		String dateString = date.format(formatter);
		System.out.println("My date is: " + dateString);
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}
