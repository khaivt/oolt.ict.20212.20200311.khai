package hust.soict.globalict.aims.utils;

import java.util.Comparator;
import java.util.Arrays;

public class DateUtils {	
	public static class DateComparator implements Comparator<MyDate> {
		@Override
		public int compare(MyDate a, MyDate b) {
			int result = a.getYear() - b.getYear();
			if (result == 0) {
				result = a.getMonth() - b.getMonth();
				if (result == 0) {
					result = a.getDay() - b.getDay();
				}
			}
			return result;
		}
	}
	
	public static int compare(MyDate a, MyDate b) {
		return (new DateComparator()).compare(a, b);
	}
	
	public static void sort(MyDate[] dates) {
		Arrays.sort(dates, new DateComparator());
	}
}
