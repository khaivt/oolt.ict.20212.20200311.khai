package hust.soict.globalict.aims;

import java.lang.Runnable;
import java.lang.Runtime;

public class MemoryDaemon implements Runnable {
	private long memoryUsed = 0;
	private boolean running = true;
	
	public void stop() {
		this.running = false;
	}
	
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		while (running) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("\tMemory used: " + used);
				memoryUsed = used;
			}
		}
	}
}
