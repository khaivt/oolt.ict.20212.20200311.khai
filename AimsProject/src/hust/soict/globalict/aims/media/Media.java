package hust.soict.globalict.aims.media;

import java.lang.Comparable;

public abstract class Media implements Comparable<Object> {
	private int id;
	protected String title;
	protected String category;
	protected float cost;
	
	public Media() {}
	
	public Media(String title) {
		this.title = title;
	}
	
	public Media(String title, String category) {
		this(title);
		this.category = category;
	}
	
	public Media(String title, String category, float cost) {
		this(title, category);
		this.cost = cost;
	}
	
	@Override
	public int compareTo(Object o) {
		if (o instanceof Media) {
			Media m = (Media)o;
			return title.compareTo(m.title);
		}
		return -1;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Media) {
			return id == ((Media)o).getId();
		}
		return false;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
}
