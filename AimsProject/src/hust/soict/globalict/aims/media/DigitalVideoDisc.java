package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {
	public DigitalVideoDisc() {}
	
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}
	
	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category, director);
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category, director, length, cost);
	}
	
	@Override
	public int compareTo(Object o) {
		if (o instanceof Disc) {
			float result = cost - ((Disc)o).getCost();
			if (result < 0) return -1;
			if (result > 0) return 1;
			return 0;
		}
		return -1;
	}
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	
	public boolean search(String title) {
		String ltitle = getTitle().toLowerCase();
		String[] tokens = title.toLowerCase().split("\\s");
		for (String token : tokens) {
			if (!ltitle.contains(token)) {
				return false;
			}
		}
		return true;
	}
}
