package hust.soict.globalict.aims.media;

public class Disc extends Media {
	protected int length;
	protected String director;
	
	public Disc() {}
	
	public Disc(String title) {
		super();
		this.title = title;
	}
	
	public Disc(String title, String category) {
		this(title);
		this.category = category;
	}
	
	public Disc(String title, String category, String director) {
		this(title, category);
		this.director = director;
	}
	
	public Disc(String title, String category, String director, int length, float cost) {
		this(title, category, director);
		this.length = length;
		this.cost = cost;
	}
	
	public int getLength() {
		return length;
	}
	
	public String getDirector() {
		return director;
	}
}
