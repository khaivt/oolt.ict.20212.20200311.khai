package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public CompactDisc(String title, String category, String artist) {
		super(title, category);
		this.artist = artist;
	}
	
	@Override
	public int compareTo(Object o) {
		if (o instanceof CompactDisc) {
			CompactDisc d = (CompactDisc)o;
			int result = tracks.size() - d.tracks.size();
			if (result < 0) return -1;
			if (result > 0) return 1;
			result = getLength() - d.getLength();
			if (result < 0) return -1;
			if (result > 0) return 1;
			return 0;
		}
		return -1;
	}
	
	public void play() {
		System.out.println("CD artist: " + artist);
		System.out.println("CD number of tracks: " + tracks.size());
		for (Track track : tracks) {
			track.play();
		}
	}
	
	public String getArtist() {
		return artist;
	}
	
	public void addTrack(Track track) {
		if (tracks.contains(track)) {
			System.out.println("Track %s have already been added".formatted(track.getTitle()));
			return;
		}
		tracks.add(track);
	}
	
	public void removeTrack(Track track) {
		if (!tracks.remove(track)) {
			System.out.println("Track %s is not on the list".formatted(track.getTitle()));
		}
	}
	
	public int getLength() {
		int length = 0;
		for (Track track : tracks) {
			length += track.getLength();
		}
		return length;
	}
}
