package hust.soict.globalict.aims.media;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Arrays;

public class Book extends Media {
	private List<String> authors = new ArrayList<String>();
	private String content = "";
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();

	public Book() {}
	
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	@Override
	public String toString() {
		return "Book: " + "title=" + title + ", category=" + category +
				", authors=" + authors + ", content length=" + contentTokens.size() +
				",\n\ttoken list=" + contentTokens +
				",\n\tword frequency=" + wordFrequency;
	}

	public String getContent() {
		return content;
	}
	
	public void processContent(String content) {
		this.content = content;
		contentTokens = Arrays.asList(content.split("[\\s.,]"));
		for (String token : contentTokens) {
			if (token.isBlank()) continue;
			wordFrequency.put(token, wordFrequency.getOrDefault(token, 0) + 1);
		}
	}
	
	public void addAuthor(String authorName) {
		for (String author : authors) {
			if (author.equals(authorName)) return;
		}
		authors.add(authorName);
	}
	
	public void removeAuthor(String authorName) {
		for (int i = 0; i < authors.size(); i += 1) {
			if (authors.get(i).equals(authorName)) {
				authors.remove(i);
				break;
			}
		}
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
}
