package hust.soict.globalict.aims.media;

import java.lang.Comparable;

public class Track implements Playable, Comparable<Object> {
	private String title;
	private int length;
	
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}
	
	@Override
	public int compareTo(Object o) {
		if (o instanceof Track) {
			Track t = (Track)o;
			return title.compareTo(t.title);
		}
		return -1;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Track) {
			Track t = (Track)o;
			return title.equals(t.getTitle()) && length == t.getLength();
		}
		return false;
	}
	
	public void play() {
		System.out.println("Playing track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}
	
	public String getTitle() {
		return title;
	}
	
	public int getLength() {
		return length;
	}
}
