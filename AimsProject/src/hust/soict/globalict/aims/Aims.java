package hust.soict.globalict.aims;

import hust.soict.globalict.aims.order.*;
import hust.soict.globalict.aims.media.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Aims extends JFrame {
	private JButton orderButton;
	private JButton addButton;
	private JComboBox<String> comboBox;
	private JLabel titleLabel;
	private JTextField titleField;
	private JLabel categoryLabel;
	private JTextField categoryField;
	private JLabel artistLabel;
	private JTextField artistField;
	private JLabel directorLabel;
	private JTextField directorField;
	private JLabel costLabel;
	private JTextField costField;
	private JTextArea textArea;

	private Order order;
	private int itemId;

	public Aims() {
		order = new Order();
		itemId = 0;

		Container cp = getContentPane();
		cp.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		orderButton = new JButton("New order");
		orderButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				order = new Order();
				textArea.setText("");
			}
		});
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.05;
		c.gridx = 0;
		c.gridy = 0;
		cp.add(orderButton, c);

		addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				String type = (String)comboBox.getSelectedItem();
				Media media;
				if (type.equals("Book")) {
					media = new Book(titleField.getText(), categoryField.getText());
				} else if (type.equals("CD")) {
					media = new CompactDisc(titleField.getText(), categoryField.getText(), artistField.getText());
				} else {
					media = new DigitalVideoDisc(titleField.getText(), categoryField.getText(), directorField.getText());
				}
				float cost;
				try {
					cost = Float.parseFloat(costField.getText());
				} catch (Exception e) {
					cost = 0;
				}
				media.setCost(cost);
				media.setId(itemId);
				itemId += 1;
				order.addMedia(media);
				textArea.setText(order.toString());
			}
		});
		c.weightx = 0.05;
		c.gridx = 1;
		c.gridy = 0;
		cp.add(addButton, c);

		comboBox = new JComboBox<>(new String[]{"Book", "CD", "DVD"});
		comboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent itemEvent) {
				String item = (String)itemEvent.getItem();
				boolean a = true;
				boolean d = true;
				if (item.equals("Book")) {
					a = d = false;
				} else if (item.equals("CD")) {
					d = false;
				} else {
					a = false;
				}
				artistField.setEditable(a);
				directorField.setEditable(d);
				if (!a) artistField.setText("");
				if (!d) directorField.setText("");
			}
		});
		c.weightx = 0.4;
		c.gridx = 2;
		c.gridy = 0;
		cp.add(comboBox, c);

		titleLabel = new JLabel("Title");
		c.weightx = 0.05;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		cp.add(titleLabel, c);
		titleField = new JTextField();
		c.weightx = 0.9;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 2;
		cp.add(titleField, c);

		categoryLabel = new JLabel("Category");
		c.weightx = 0.05;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		cp.add(categoryLabel, c);
		categoryField = new JTextField();
		c.weightx = 0.9;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 2;
		cp.add(categoryField, c);

		artistLabel = new JLabel("Artist");
		c.weightx = 0.05;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		cp.add(artistLabel, c);
		artistField = new JTextField();
		artistField.setEditable(false);
		c.weightx = 0.9;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 2;
		cp.add(artistField, c);

		directorLabel = new JLabel("Director");
		c.weightx = 0.05;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 1;
		cp.add(directorLabel, c);
		directorField = new JTextField();
		directorField.setEditable(false);
		c.weightx = 0.9;
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth = 2;
		cp.add(directorField, c);

		costLabel = new JLabel("Cost");
		c.weightx = 0.05;
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 1;
		cp.add(costLabel, c);
		costField = new JTextField();
		c.weightx = 0.9;
		c.gridx = 1;
		c.gridy = 5;
		c.gridwidth = 2;
		cp.add(costField, c);

		textArea = new JTextArea();
		textArea.setEditable(false);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 0.5;
		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 3;
		cp.add(textArea, c);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Aims");
		setSize(800, 500);
		setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Aims();
			}
		});
	}
}
