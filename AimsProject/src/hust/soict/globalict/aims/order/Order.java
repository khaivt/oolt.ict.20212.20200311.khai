package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.*;

import java.util.ArrayList;

public class Order extends Object {
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;
	
	private int id;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private MyDate dateOrdered;
	private int luckyItem;
	
	public Order() {
		if (nbOrders >= MAX_LIMITED_ORDERS) {
			System.out.println("Maximum number of orders");
			return;
		}
		nbOrders += 1;
		dateOrdered = new MyDate();
		luckyItem = -1;
	}
	
	public void getALuckyItem() {
		luckyItem = (int)(Math.random() * itemsOrdered.size());
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(String.format("Date: %d-%d-%d",
				dateOrdered.getDay(), dateOrdered.getMonth(), dateOrdered.getYear()));
		str.append("\nOrdered Items:\n");
		for (int i = 0; i < itemsOrdered.size(); i += 1) {
			String free = "";
			if (i == luckyItem) free = "[Free] ";
			Media media = itemsOrdered.get(i);
			String line = String.format(i + 1 + ". %sId: %d - Title: %s - Category: %s - Cost: %.2f$\n",
					free, media.getId(), media.getTitle(),
					media.getCategory(), media.getCost());
			str.append(line);
		}
		str.append("Total cost: " + totalCost());
		return str.toString();
	}

	public void print() {
		System.out.println("********************Order********************");
		String date = String.format("Date: %d-%d-%d",
				dateOrdered.getDay(), dateOrdered.getMonth(), dateOrdered.getYear());
		System.out.println(date);
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); i += 1) {
			String free = "";
			if (i == luckyItem) free = "[Free] ";
			Media media = itemsOrdered.get(i);
			String line = String.format(i + 1 + ". %sId: %d - Title: %s - Category: %s - Cost: %.2f$",
					free, media.getId(), media.getTitle(),
					media.getCategory(), media.getCost());
			System.out.println(line);
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("*********************************************");
	}
	
	public void addMedia(Media media) {
		itemsOrdered.add(media);
	}
	
	public void addMedia(Media ... medias) {
		for (int i = 0; i < medias.length; i += 1) {
			addMedia(medias[i]);
		}
	}
	
	public void removeMedia(int id) {
		for (int i = 0; i < itemsOrdered.size(); i += 1) {
			if (itemsOrdered.get(i).getId() == id) {
				itemsOrdered.remove(i);
				break;
			}
		}
	}
	
	public void removeMedia(Media media) {
		for (int i = 0; i < itemsOrdered.size(); i += 1) {
			if (itemsOrdered.get(i).getTitle() == media.getTitle()) {
				itemsOrdered.remove(i);
				break;
			}
		}
	}
	
	public float totalCost() {
		float cost = 0;
		for (int i = 0; i < itemsOrdered.size(); i += 1) {
			if (i == luckyItem) continue;
			cost += itemsOrdered.get(i).getCost();
		}
		return cost;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
