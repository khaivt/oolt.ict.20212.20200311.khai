package hust.soict.globalict.lab02;

import java.util.Scanner;

public class Star {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 0; i < n; i += 1) {
			for (int j = n - i - 1; j > 0; j -= 1) {
				System.out.print(' ');
			}
			for (int j = 1 + 2 * i; j > 0; j -= 1) {
				System.out.print('*');
			}
			System.out.println();
		}
		scanner.close();
	}

}
