package hust.soict.globalict.lab02;

import java.util.Scanner;
import java.util.Arrays;

public class Sort {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input array length: ");
		int n = scanner.nextInt(), sum = 0;
		double avg = 0;
		int a[] = new int[n];
		System.out.println("Input array element(s):");
		for (int i = 0; i < n; i += 1) {
			a[i] = scanner.nextInt();
			sum += a[i];
			avg += a[i];
		}
		avg /= n;
		Arrays.sort(a);
		System.out.print("Sorted array is: ");
		for (int i = 0; i < n; i += 1) System.out.print(a[i] + " ");
		System.out.println("\nSum of array elements: " + sum);
		System.out.println("Average of array elements: " + avg);
		scanner.close();
	}
}
