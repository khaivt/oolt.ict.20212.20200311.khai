package hust.soict.globalict.lab02;

import java.util.Scanner;

public class Month {
	static int getMonth(String month) {
		String longNames[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		String abbrNames[] = {"Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec."};
		String shortNames[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		for (int i = 0; i < 12; i += 1) {
			if (longNames[i].equals(month) ||
				abbrNames[i].equals(month) ||
				shortNames[i].equals(month) ||
				String.valueOf(i + 1).equals(month)) {
				return i + 1;
			}
		}
		return -1;
	}
	
	static int getYear(String year) {
		int value = 0;
		for (int i = 0; i < year.length(); i += 1) {
			char c = year.charAt(i);
			if (c < '0' || c > '9') return -1;
			value = value * 10 + c - '0';
		}
		return value;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int month = -1;
		while (month < 0) {
			System.out.print("Input month: ");
			month = getMonth(scanner.next());
		}
		int year = -1;
		while (year < 0) {
			System.out.print("Input year: ");
			year = getYear(scanner.next());
		}
		int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) days[1] += 1;
		System.out.print("Number of days: " + days[month - 1]);
		scanner.close();
	}
}
