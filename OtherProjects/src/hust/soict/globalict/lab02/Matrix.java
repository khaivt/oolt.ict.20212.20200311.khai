package hust.soict.globalict.lab02;

import java.util.Scanner;

public class Matrix {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input matrix dimension: ");
		int m = scanner.nextInt(), n = scanner.nextInt();
		int a[][] = new int[m][n];
		int b[][] = new int[m][n];
		System.out.println("Input first matrix: ");
		for (int i = 0; i < m; i += 1) {
			for (int j = 0; j < n; j += 1) {
				a[i][j] = scanner.nextInt();
			}
		}
		System.out.println("Input second matrix: ");
		for (int i = 0; i < m; i += 1) {
			for (int j = 0; j < n; j += 1) {
				b[i][j] = scanner.nextInt();
			}
		}
		System.out.println("Sum of two matrices is: ");
		for (int i = 0; i < m; i += 1) {
			for (int j = 0; j < n; j += 1) {
				System.out.print((a[i][j] + b[i][j]) + " ");
			}
			System.out.println();
		}
		scanner.close();
	}
}
