package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
import java.lang.Math;

public class EquationSolver {
    static void case1() {
	String prefix = "Equation: ax + b = 0\n", input;
	double a, b;
	while (true) {
	    input = JOptionPane.showInputDialog(null, prefix + "Input a(# 0):", "Input", JOptionPane.INFORMATION_MESSAGE);
	    a = Double.parseDouble(input);
	    if (a != 0) break;
	}
	input = JOptionPane.showInputDialog(null, prefix + "Input b:", "Input", JOptionPane.INFORMATION_MESSAGE);
	b = Double.parseDouble(input);
	JOptionPane.showMessageDialog(null, prefix + "Solution: x = " + (-b / a), "Result", JOptionPane.INFORMATION_MESSAGE);
    }

    static void case2() {
	String prefix = "System of equations:\n" + "ax + by = c\n" + "dx + ey = f\n";
	String input;
	double[][] x = new double[2][3];
	String[][] var = {{"a", "b", "c"}, {"d", "e", "f"}};
	for (int i = 0; i < 2; ++i) {
	    for (int j = 0; j < 3; ++j) {
		input = JOptionPane.showInputDialog(null, prefix + "Input " + var[i][j] + ":", "Input", JOptionPane.INFORMATION_MESSAGE);
		x[i][j] = Double.parseDouble(input);
	    }
	}
	double D = x[0][0] * x[1][1] - x[1][0] * x[0][1];
	double D1 = x[0][2] * x[1][1] - x[1][2] * x[0][1];
	double D2 = x[1][2] * x[0][0] - x[0][2] * x[1][0];
	if (D == 0) {
	    if (D1 == 0 && D2 == 0) JOptionPane.showMessageDialog(null, prefix + "Infinitely many solutions", "Result", JOptionPane.INFORMATION_MESSAGE);
	    else JOptionPane.showMessageDialog(null, prefix + "No solution", "Result", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(null, prefix + "Solution: x = " + (D1 / D) + "; y = " + (D2 / D), "Result", JOptionPane.INFORMATION_MESSAGE);
	}
    }

    static void case3() {
	String prefix = "Equation: ax + by + c = 0\n";
	String input;
	double a, b, c;
	while (true) {
	    input = JOptionPane.showInputDialog(null, prefix + "Input a(# 0):", "Input", JOptionPane.INFORMATION_MESSAGE);
	    a = Double.parseDouble(input);
	    if (a != 0) break;
	}
	input = JOptionPane.showInputDialog(null, prefix + "Input b:", "Input", JOptionPane.INFORMATION_MESSAGE);
	b = Double.parseDouble(input);
	input = JOptionPane.showInputDialog(null, prefix + "Input c:", "Input", JOptionPane.INFORMATION_MESSAGE);
	c = Double.parseDouble(input);
	double delta = b * b - 4 * a * c;
	if (delta < 0) {
	    JOptionPane.showMessageDialog(null, prefix + "No solution", "Result", JOptionPane.INFORMATION_MESSAGE);
	} else if (delta == 0) {
	    JOptionPane.showMessageDialog(null, prefix + "Double root: x1 = x2 = " + (-b / (2 * a)), "Result", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    double x1 = (-b + Math.sqrt(delta)) / (2 * a);
	    double x2 = (-b - Math.sqrt(delta)) / (2 * a);
	    JOptionPane.showMessageDialog(null, prefix + "Solution: x1 = " + x1 + "; x2 = " + x2, "Result", JOptionPane.INFORMATION_MESSAGE);
	}
    }
    
    public static void main(String[] args) {
	String option;
	String message = "Find solution for:\n" + 
	    "1. First-degree equation\n" +
	    "2. System of first-degree equation\n" +
	    "3. Second-degree equation";
	while (true) {
	    option = JOptionPane.showInputDialog(null, message, "Choose option", JOptionPane.INFORMATION_MESSAGE);
	    if (option.equals("1")) {
		case1();
		break;
	    } else if (option.equals("2")) {
		case2();
		break;
	    } else if (option.equals("3")) {
		case3();
		break;
	    }
	}
    }
}
