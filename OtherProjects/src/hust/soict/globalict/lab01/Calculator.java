package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class Calculator {
    public static void main(String[] args) {
	String strNum1, strNum2;
	strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
	strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
	
	double num1 = Double.parseDouble(strNum1);
	double num2 = Double.parseDouble(strNum2);
	String message = "Sum: " + (num1 + num2) + "\n";
	message += "Difference: " + (num1 - num2) + "\n";
	message += "Product: " + (num1 * num2) + "\n";
	if (num2 == 0) message += "Division: Divisor is equal to zero";
	else message += "Division: " + (num1 / num2);
	JOptionPane.showMessageDialog(null, message, "Result", JOptionPane.INFORMATION_MESSAGE);
    }
}
