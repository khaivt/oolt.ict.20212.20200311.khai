package hust.soict.globalict.garbage;

import java.util.Random;
import java.io.File;
import java.util.Scanner;

public class ConcatenationInLoops {
	public static void main(String[] args) {
		Random r = new Random();
		long start = System.currentTimeMillis();
		String s = "";
		for (int i = 0; i < 4000; i += 1) {
			s += r.nextInt(2);
		}
		System.out.println("Using String: " + (System.currentTimeMillis() - start));
		
		r = new Random(100);
		start = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 4000; i += 1) {
			sb.append(r.nextInt(2));
		}
		s = sb.toString();
		System.out.println("Using StringBuilder: " + (System.currentTimeMillis() - start));
		
		// Read file
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input file path: ");
		s = scanner.nextLine();
		scanner.close();
		File file = new File(s);
//		if (!file.exists()) {
//			System.out.println("File does not exist");
//			return;
//		}
		// Use string
		start = System.currentTimeMillis();
		try (Scanner sc = new Scanner(file)) {
			s = "";
			while (sc.hasNextByte()) {
				s += sc.nextByte();
			}
		} catch (Exception e) {
			System.out.println("Cannot read file");
			return;
		}
		System.out.println("Using String to read file: " + (System.currentTimeMillis() - start));
		// Use string
		start = System.currentTimeMillis();
		try (Scanner sc = new Scanner(file)) {
			sb = new StringBuilder();
			while (sc.hasNextByte()) {
				sb.append(sc.nextByte());
			}
		} catch (Exception e) {
			System.out.println("Cannot read file");
			return;
		}
		System.out.println("Using StringBuilder to read file: " + (System.currentTimeMillis() - start));
	}
}
