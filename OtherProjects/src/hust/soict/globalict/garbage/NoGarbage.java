package hust.soict.globalict.garbage;

public class NoGarbage {
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		long start = System.currentTimeMillis();
		while (true) {
			sb.append('a');
			if (sb.length() > 10000) break;
		}
		System.out.println("Runtime: " + (System.currentTimeMillis() - start));
	}
}
