package hust.soict.globalict.garbage;

public class GarbageCreator {
	public static void main(String[] args) {
		String s = "";
		long start = System.currentTimeMillis();
		while (true) {
			String newStr = s + 'a';
			s = newStr;
			if (newStr.length() > 10000) break;
		}
		System.out.println("Runtime: " + (System.currentTimeMillis() - start));
	}
}
